import re
import httpx
import asyncio


def url_parser(webpage, anime, separator):

    return webpage + anime.lower().replace(" ", separator)


def get_target_data(url, target):

    SUCCESS = 200
    q = httpx.get(url)
    if q.status_code == SUCCESS:
        links = re.findall(target, q.text)
        links.reverse()
        return links
    return None


async def get_imgs_requests(urls):

    gather_reqs = list()
    aux_array = [urls[x : x + 50] for x in range(0, len(urls), 50)]
    for chunk in aux_array:
        try:
            async with httpx.AsyncClient() as client:
                tasks = (client.get(data["url"]) for data in chunk)
                reqs = await asyncio.gather(*tasks)
            gather_reqs.extend(reqs)
        except httpx.ReadTimeout:
            pass

    return gather_reqs


def create_data_scheme(manga, manga_url):

    # manga_url = url_parser(url, manga, sep)

    manga_data = {
        "name": manga,
        "manga_url": manga_url,
        "total_chaps": 0,
        "chapters": list(),
    }

    return manga_data


def get_chapter_list(manga_data, target, link_end):

    if chaps := get_target_data(manga_data["manga_url"], target):
        if (new_length := len(chaps)) > manga_data["total_chaps"]:
            chaps = chaps[manga_data["total_chaps"] :]
            manga_data["total_chaps"] = new_length

        for chap in chaps:
            data = {
                "name": chap[1],  # TODO: format name HYML Entities
                "total_pages": 0,
                "url": chap[0] + link_end,
            }
            manga_data["chapters"].append(data)


def get_imgs_urls(manga_data, target):

    aux_chaps = [entry for entry in manga_data["chapters"] if not entry["total_pages"]]

    if aux_chaps:
        reqs = asyncio.run(get_imgs_requests(aux_chaps))

        for req, data in zip(reqs, aux_chaps):
            # get chapter imgs urls
            imgs = re.findall(target, req.text)
            data["total_pages"] = len(imgs)
            data["pages_urls"] = [
                {"url": url}
                for url in imgs if url.startswith("https://")
            ]


if __name__ == "__main__":

    import json

    sources = {
        "mangainn": {
            "url_append": "/all-pages/",
            "chap_target": '<a href="(.*?)">\n<span class="val">(.*?) <\/span>',
            "img_target": '<img src="(.*?)" class="img-responsive">',
        },
        "holymanga": {
            "url_append": str(),
            "chap_target": '<h2 class="chap"><a href="(.*?)">(.*?) <span style=',
            "img_target": '<img src="(.*?)"',
            # TODO: Needs to search through pages of chapters
        },
    }

    name = "One Piece"
    url = "https://www.mangainn.net/one-piece1"
    # url = "https://www.mangainn.net/please-dont-bully-me-nagatoro"
    # url = "https://w27.holymanga.net/please-dont-bully-me-nagatoro/page-0/"
    source = "mangainn"

    data = create_data_scheme(name, url)
    if data:
        get_chapter_list(
            data, sources[source]["chap_target"], sources[source]["url_append"]
        )
        get_imgs_urls(data, sources[source]["img_target"])
        get_imgs_urls(data, sources[source]["img_target"])  # Twice for too long mangas
        with open("new.json", "w") as f:
            json.dump(data, f)
