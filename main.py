import os
import json
import httpx
import asyncio
from manga.manga_tools import *


if __name__ == "__main__":

    manga_dir_name = "Manga/"
    parent_dir = "/home/awe/"
    path = os.path.join(parent_dir, manga_dir_name)

    try:
        os.mkdir(path)
        print(f"{manga_dir_name} dir made!")
    except FileExistsError:
        print(f"{manga_dir_name} already made!")

    with open('one_piece.json', 'r') as f:
        data = json.load(f)

    manga_path = os.path.join(path, data["name"] + "/")

    try:
        os.mkdir(manga_path)
        print(f"{manga_path} dir made!")
    except FileExistsError:
        print(f"{manga_path} already made!")

    for chap in data["chapters"]:
        chapt_path = os.path.join(manga_path, chap["name"] + "/")
        try:
            os.mkdir(chapt_path)
            print(f"{chapt_path} dir made!")
            reqs = asyncio.run(get_imgs_requests(chap['pages_urls']))
            print("Done fetching imgs")

            for req in reqs:
                file_name = str(req.url).split("/")[-1]
                file_path = os.path.join(chapt_path, file_name)
                with open(file_path, "wb") as f:
                    f.write(req.content)
        except FileExistsError:
            print(f"{chapt_path} already made!")
        
